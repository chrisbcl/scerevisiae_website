var mongo = require('mongodb');

var Server = mongo.Server,
    Db = mongo.Db,
    BSON = mongo.BSONPure;

var server = new Server('localhost', 27017, {auto_reconnect: true});
db = new Db('sgddb', server, {safe: true});

db.open(function(err, db){
    if (!err){
        console.log("Connected to 'sgddb' database");
        db.collection('sgdgenes', {safe: true}, function(err, collection){
            if (err){
                console.log("Creating 'sgdgenes' and populating");
                populateDbGenes();
            }
        });
        db.collection('reactions', {safe:true}, function(err, collection){
            if (err){
                console.log("Creating 'reactions' and populating");
                populateDbReactions();
            }
        });
        db.collection('species', {safe:true}, function(err, collection){
            if (err){
                console.log("Creating 'species' and populating");
                populateDbSpecies();
            }
        });
        db.collection('metabolites', {safe:true}, function(err, collection){
            if (err){
                console.log("Creating 'metabolites' and populating");
                populateDbMetabolites();
            }
        });
        db.collection('gprs', {safe: true}, function(err, collection){
            if (err){
                console.log("Creating 'gprs' and populating");
                populateDbGPRs();
            }
        });
    }
});

exports.findGeneByEntry = function(req, res){
    var entry = req.params.entry;
    console.log("Retrieving gene " + entry);
    db.collection('sgdgenes', function(err, collection){
        collection.findOne({ 'entry': entry}, function(err, item){
            res.send(item);
        }); 
    });
};

exports.findAllGenes = function(req, res){
    db.collection('sgdgenes', function(err, collection){
        collection.find().toArray(function(err, items){
            res.send(items);
        });
    });
};

exports.findReactionByEntry = function(req, res){
    var model = req.params.model;
    var reaction = req.params.reaction;
    var entry = reaction + '@' + model;
    db.collection('reactions', function(err, collection){
        collection.findOne({'entry': entry}, function(err, item){
            res.send(item);
        });
    });
};

exports.findAllReactions = function(req, res){
    db.collection('reactions', function(err, collection){
        collection.find().toArray(function(err, items){
            res.send(items);
        });
    });
};

exports.findSpecieByEntry = function(req, res){
    var model = req.params.model;
    var specie = req.params.specie;
    var entry = specie + '@' + model;
    db.collection('species', function(err, collection){
        collection.findOne({'entry': entry}, function(err, item){
            res.send(item);
        });
    });
};

exports.findAllSpecies = function(req, res){
    db.collection('species', function(err, collection){
        collection.find().toArray(function(err, items){
            res.send(items);
        });
    });
};

exports.findMetaboliteByEntry = function(req, res){
    var model = req.params.model;
    var metab = req.params.metabolite;
    var entry = metab + '@' + model;
    db.collection('metabolites', function(err, collection){
        collection.findOne({'entry': entry}, function(err, item){
            res.send(item);
        });
    });
};

exports.findAllMetabolites = function(req, res){
    db.collection('metabolites', function(err, collection){
        collection.find().toArray(function(err, items){
            res.send(items);
        });
    });
};

exports.findGPRByReaction = function(req, res){
    var reaction = req.params.reaction;
    var model = req.params.model;
    var entry = reaction + "@" + model;
    db.collection('gprs', function(err, collection){
        collection.findOne({'reaction' : entry}, function(err, item){
            res.send(item);
        });
    });
};


var populateDbGenes = function(){
    var genes = [
    {
        entry:"S000000514",
        proxy:false,
        name:"STerile Pseudoreversion",
        major_label:"SGD",
        status:"Active",
        alias:"ubiquitin-binding ESCRT-I subunit protein STP22 VPS23 AGS1 VPL15",
        featureType:"ORF",
        length:"1158",
        systematic_name:"YCL008C",
        standardName:"STP22",
        organism:"S. cerevisiae",
        phenotypes: ["Viable","Resistance to chemicals","Competitive fitness","Auxotrophy"],
        gprs: ["R_C4STMO2@iMM904"],
        uniprot: null,
        expazy: null
    },
    {
        entry:"S000000515",
        proxy:false,
        name:"IsoLeucine Valine",
        major_label:"SGD",
        status:"Active",
        alias:"acetolactate synthase regulatory subunit",
        featureType:"ORF",
        length:"930",
        systematic_name:"YCL009C",
        standardName:"ILV6",
        organism:"S. cerevisiae",
        phenotypes: ["Viable","Toxin resistance","Metal resistance","Heat sensitivity","Hyperosmotic stress resistance","Haploproficient","Vegetative growth","Chemical compound accumulation","Resistance to chemicals","Protein/peptide modification","Utilization of nitrogen source","Competitive fitness","Innate thermotolerance","Chronological lifespan","Protein transport","Lipid particle morphology"],
        gprs: ["R_G5SADrm@iMM904"],
        uniprot: null,
        expazy: null
    },
    {
        entry:"S000000516",
        proxy:false,
        name:"SaGa associated Factor",
        major_label:"SGD",
        status:"Active",
        alias:"null",
        featureType:"ORF",
        length:"780",
        systematic_name:"YCL010C",
        standardName:"SGF29",
        organism:"S. cerevisiae",
        phenotypes: ["Telomere length","Toxin resistance","Resistance to chemicals","Competitive fitness","Starvation resistance","Inviable","Filamentous growth"],
        gprs: ["R_GLUPRT@iMM904"],
        uniprot: null,
        expazy: null
    },                                                                                  
    {
        entry:"S000000517",
        proxy:false,
        name:"G-strand Binding Protein",
        major_label:"SGD",
        status:"Active",
        alias:"RLF6",
        featureType:"ORF",
        length:"1284",
        systematic_name:"YCL011C",
        standardName:"GBP2",
        organism:"S. cerevisiae",
        phenotypes: ["Resistance to chemicals","Competitive fitness","Protein/peptide accumulation","Colony sectoring","Inviable","Mitochondrial morphology"],
        gprs: ["R_PDE1@iMM904"],
        uniprot: null,
        expazy: null
    },                                                                                
    {
        entry:"S000029705",
        proxy:false,
        name:"null",
        major_label:"SGD",
        status:"Active",
        alias:"YCL011C-A",
        featureType:"ORF",
        length:"472",
        systematic_name:"YCL012C",
        standardName:"null",
        organism:"S. cerevisiae",
        phenotypes: ["Viable","Protein secretion","Metal resistance","Chemical compound excretion","Chemical compound accumulation","Protein/peptide distribution","Resistance to chemicals","Desiccation resistance","Vacuolar transport","Protein/peptide modification","Competitive fitness","Sporulation efficiency","Sporulation","Protein/peptide accumulation","Chronological lifespan","Protein activity","Filamentous growth","Nuclear morphology","Autophagy","Viability","Mitophagy","Pexophagy"],
        gprs: ["R_C4STMO1@iMM904"],
        uniprot: null,
        expazy: null
    },                                                                                                
    {
        entry:"S000000520",
        proxy:false,
        name:"BUD site selection",
        major_label:"SGD",
        status:"Active",
        alias:"YCL012W",
        featureType:"ORF",
        length:"4911",
        systematic_name:"YCL014W",
        standardName:"BUD3",
        organism:"S. cerevisiae",
        phenotypes: ["Viable","Competitive fitness","Silencing","Haploinsufficient"],
        gprs: ["R_4HBZFm@iMM904"],
        uniprot: null,
        expazy: null
    },                                                                                   
    {
        entry:"S000000521",
        proxy:false,
        name:"Defective in sister Chromatid Cohesion",
        major_label:"SGD",
        status:"Active",
        alias:"null",
        featureType:"ORF",
        length:"1143",
        systematic_name:"YCL016C",
        standardName:"DCC1",
        organism:"S. cerevisiae",
        phenotypes: ["Viable","Chemical compound accumulation","Resistance to chemicals","Utilization of nitrogen source","Competitive fitness","Auxotrophy"],
        gprs: ["R_PDE3@iMM904"],
        uniprot: null,
        expazy: null
    },                                                                  
    {
        entry:"S000000522",
        proxy:false,
        name:"NiFS-like",
        major_label:"SGD",
        status:"Active",
        alias:"SPL1",
        featureType:"ORF",
        length:"1494",
        systematic_name:"YCL017C",
        standardName:"NFS1",
        organism:"S. cerevisiae",
        phenotypes: ["Viable","Metal resistance","Haploproficient","Vacuolar morphology","Resistance to chemicals","Desiccation resistance","Competitive fitness","Chromosome/plasmid maintenance","Chronological lifespan","Protein transport","Actin cytoskeleton morphology"],
        gprs: ["R_PDE2@iMM904"],
        uniprot: null,
        expazy: null
    },                                                                                               
    {
        entry:"S000000523",
        proxy:false,
        name:"LEUcine biosynthesis",
        major_label:"SGD",
        status:"Active",
        alias:"3-isopropylmalate dehydrogenase",
        featureType:"ORF",
        length:"1095",
        systematic_name:"YCL018W",
        standardName:"LEU2",
        organism:"S. cerevisiae",
        phenotypes: ["Viable","Resistance to chemicals","Competitive fitness","Starvation resistance"],
        gprs: ["R_METTRSm@iMM904"],
        uniprot: null,
        expazy: null
    },                                                         
    {
        entry:"S000007549",
        proxy:false,
        name:"null",
        major_label:"SGD",
        status:"Active",
        alias:"null",
        featureType:"ORF",
        length:"378",
        systematic_name:"YCL021W-A",
        standardName:"null",
        organism:"S. cerevisiae",
        phenotypes: ["Viable","Resistance to chemicals","Competitive fitness"],
        gprs: ["R_PUNP6@iMM904"],
        uniprot: null,
        expazy: null
    },                                                                                                   
    {
        entry:"S000000527",
        proxy:false,
        name:"null",
        major_label:"SGD",
        status:"Active",
        alias:"null",
        featureType:"ORF",
        length:"516",
        systematic_name:"YCL022C",
        standardName:"null",
        organism:"S. cerevisiae",
        phenotypes: ["Viable","Resistance to chemicals","Utilization of nitrogen source","Competitive fitness"],
        gprs: ["R_PUNP7@iMM904"],
        uniprot: null,
        expazy: null
    },                                                                                                     
    {
        entry:"S000000529",
        proxy:false,
        name:"null",
        major_label:"SGD",
        status:"Active",
        alias:"null",
        featureType:"ORF",
        length:"3114",
        systematic_name:"YCL024W",
        standardName:"KCC4",
        organism:"S. cerevisiae",
        phenotypes: ["Oxidative stress resistance","Competitive fitness","Haploinsufficient","Inviable","Cold sensitivity"],
        gprs: ["R_FA120ACPH@iMM904"],
        uniprot: null,
        expazy: null
    },                                                                                                    
    {
        entry:"S000002323",
        proxy:false,
        name:"Cell Division Cycle",
        major_label:"SGD",
        status:"Active",
        alias:"MMS8 DNA ligase (ATP) CDC9",
        featureType:"ORF",
        length:"2268",
        systematic_name:"YDL164C",
        standardName:"CDC9",
        organism:"S. cerevisiae",
        phenotypes: ["Resistance to chemicals","Competitive fitness","Starvation resistance","Protein/peptide accumulation","Chromosome/plasmid maintenance","Inviable","Cell cycle progression in S phase"],
        gprs: ["R_PUNP4@iMM904"],
        uniprot: null,
        expazy: null
    },                                                               
    {
        entry:"S000002325",
        proxy:false,
        name:"Factor Activating Pos9",
        major_label:"SGD",
        status:"Active",
        alias:"null",
        featureType:"ORF",
        length:"594",
        systematic_name:"YDL166C",
        standardName:"FAP7",
        organism:"S. cerevisiae",
        phenotypes: ["Chemical compound excretion","Protein/peptide distribution","Resistance to chemicals","Competitive fitness","Starvation resistance","Silencing","Protein/peptide accumulation","Inviable","Nuclear morphology","Cold sensitivity"],
        gprs: ["R_PUNP5@iMM904"],
        uniprot: null,
        expazy: null
    },                                                                                   
    {
        entry:"S000002326",
        proxy:false,
        name:"N (asparagine)-Rich Protein",
        major_label:"SGD",
        status:"Active",
        alias:"null",
        featureType:"ORF",
        length:"2160",
        systematic_name:"YDL167C",
        standardName:"NRP1",
        organism:"S. cerevisiae",
        phenotypes: ["Viable","Oxidative stress resistance","Competitive fitness"],
        gprs: ["R_GLYCLm@iMM904"],
        uniprot: null,
        expazy: null
    },                                                                             
    {
        entry:"S000002327",
        proxy:false,
        name:"Sensitive to FormAldehyde",
        major_label:"SGD",
        status:"Active",
        alias:"ADH5 bifunctional alcohol dehydrogenase/S-(hydroxymethyl)glutathione dehydrogenase",
        featureType:"ORF",
        length:"1161",
        systematic_name:"YDL168W",
        standardName:"SFA1",
        organism:"S. cerevisiae",
        phenotypes: ["Viable","Respiratory growth","Heat sensitivity","Haploproficient","Vegetative growth","Chemical compound accumulation","Resistance to chemicals","Desiccation resistance","Competitive fitness","UV resistance","Mutation frequency","Chronological lifespan","Mitochondrial morphology","Mitochondrial genome maintenance","Stress resistance"],
        gprs: ["R_FCLTm@iMM904"],
        uniprot: null,
        expazy: null
    }, 
    {
        entry:"S000002328",
        proxy:false,
        name:"Unidentified Gene X",
        major_label:"SGD",
        status:"Active",
        alias:"null",
        featureType:"ORF",
        length:"672",
        systematic_name:"YDL169C",
        standardName:"UGX2",
        organism:"S. cerevisiae",
        phenotypes: ["Viable","Vegetative growth","Chemical compound accumulation","Resistance to chemicals","Competitive fitness","Starvation resistance","Redox state"],
        gprs: ["R_FUMAC@iMM904"],
        uniprot: null,
        expazy: null
    },                                                                                      
    {
        entry:"S000002330",
        proxy:false,
        name:"GLuTamate synthase",
        major_label:"SGD",
        status:"Active",
        alias:"glutamate synthase (NADH)",
        featureType:"ORF",
        length:"6438",
        systematic_name:"YDL171C",
        standardName:"GLT1",
        organism:"S. cerevisiae",
        phenotypes: ["Viable","Chemical compound accumulation","Vacuolar morphology","Resistance to chemicals","Competitive fitness","Haploinsufficient"],
        gprs: ["R_HCYSMT@iMM904"],
        uniprot: null,
        expazy: null
    },                                                                 
    {
        entry:"S000000383",
        proxy:false,
        name:"FuZzy Onions homolog",
        major_label:"SGD",
        status:"Active",
        alias:"mitofusin",
        featureType:"ORF",
        length:"2568",
        systematic_name:"YBR179C",
        standardName:"FZO1",
        organism:"S. cerevisiae",
        phenotypes: ["Viable","Oxidative stress resistance","Haploproficient","Vegetative growth","Resistance to chemicals","Competitive fitness","Starvation resistance","Chromosome/plasmid maintenance","Inviable","Stress resistance","Protein transport"],
        gprs: ["R_METTRS@iMM904"],
        uniprot: null,
        expazy: null
    },                                                                               
    {
        entry:"S000000384",
        proxy:false,
        name:"DiTyRosine",
        major_label:"SGD",
        status:"Active",
        alias:"null",
        featureType:"ORF",
        length:"1719",
        systematic_name:"YBR180W",
        standardName:"DTR1",
        organism:"S. cerevisiae",
        phenotypes: ["Viable","Chemical compound excretion","Vegetative growth","Chemical compound accumulation","Vacuolar morphology","Resistance to chemicals","Desiccation resistance","Competitive fitness","Inviable","Cold sensitivity"],
        gprs: ["R_THMDt2@iMM904"],
        uniprot: null,
        expazy: null
    }];                                                 
    
    db.collection('sgdgenes', function(err, collection){
        collection.insert(genes, {safe: true}, function(err, result) {});
    });

};

var populateDbReactions = function(){
    var reactions = [
    {entry:"R_GLUPRT@iIT341",
        proxy:false,
        name:"glutamine phosphoribosyldiphosphate amidotransferase",
        description:"",
        source:"/home/fliu/data/sbm/iIT341.xml",
        orientation:"LeftToRight",
        translocation:false,
        updated_at:1426458341487,
        created_at:1426458341487,
        containerReversible:false,
        upperBound:999999.0,
        geneRule:"",
        lowerBound:0.0,         
        model: "iIT341",
        reactants: [{specie: 'specie1', stoichiometry: 1}, {specie: 'specie2', stoichiometry: 2}],
        products: [{specie:'specie3', stoichiometry: 1}, {specie: 'specie4', stoichiometry: 3}]
    },                                                                                                         
    {entry:"R_UAAGDS@iIT341",
        proxy:false,
        name:"UDP-N-acetylmuramoyl-L-alanyl-D-glutamyl-meso-2,6-diaminopimelate synthetase",
        description:"",
        source:"/home/fliu/data/sbm/iIT341.xml",
        orientation:"LeftToRight",
        translocation:false,
        updated_at:1426458341539,
        created_at:1426458341539,
        containerReversible:false,
        upperBound:999999.0,
        geneRule:"HP1494",
        lowerBound:0.0, 
        model: "iIT341",   
        reactants: [{specie: 'specie1', stoichiometry: 1}, {specie: 'specie2', stoichiometry: 2}],
        products: [{specie:'specie3', stoichiometry: 1}, {specie: 'specie4', stoichiometry: 3}]
    },                                                                                    
    {entry:"R_ATPS4r@iIT341",
        proxy:false,
        name:"ATP synthase (four protons for one ATP)",
        description:"",
        source:"/home/fliu/data/sbm/iIT341.xml",
        orientation:"Reversible",
        translocation:true,
        updated_at:1426458341544,
        created_at:1426458341544,
        containerReversible:true,
        upperBound:999999.0,
        geneRule:"( ( ( ( ( ( ( HP1212 and HP1131 ) and ( HP1136 and HP1137 ) ) and HP1135 ) and HP1132 ) and HP1133 ) and HP0828 ) and HP1134 )",
        lowerBound:-999999.0, 
        model: "iIT341",
        reactants: [{specie: 'specie1', stoichiometry: 1}, {specie: 'specie2', stoichiometry: 2}],
        products: [{specie:'specie3', stoichiometry: 1}, {specie: 'specie4', stoichiometry: 3}]
    }, 
    {entry:"R_PUNP6@iIT341",
        proxy:false,
        name:"purine-nucleoside phosphorylase (Deoxyinosine)",
        description:"",
        source:"/home/fliu/data/sbm/iIT341.xml",
        orientation:"Reversible",
        translocation:false,
        updated_at:1426458341548,
        created_at:1426458341548,
        containerReversible:true,
        upperBound:999999.0,
        geneRule:"HP1178",
        lowerBound:-999999.0,                                                                                                                  
        model: "iIT341",
        reactants: [{specie: 'specie1', stoichiometry: 1}, {specie: 'specie2', stoichiometry: 2}],
        products: [{specie:'specie3', stoichiometry: 1}, {specie: 'specie4', stoichiometry: 3}]
    },
    {entry:"R_PUNP7@iIT341",
        proxy:false,
        name:"purine-nucleoside phosphorylase (Xanthosine)",
        description:"",
        source:"/home/fliu/data/sbm/iIT341.xml",
        orientation:"Reversible",
        translocation:false,
        updated_at:1426458341550,
        created_at:1426458341550,
        containerReversible:true,
        upperBound:999999.0,
        geneRule:"HP1178",
        lowerBound:-999999.0,                                                                 
        model: "iIT341",
        reactants: [{specie: 'specie1', stoichiometry: 1}, {specie: 'specie2', stoichiometry: 2}],
        products: [{specie:'specie3', stoichiometry: 1}, {specie: 'specie4', stoichiometry: 3}]
    },                                                   
    {entry:"R_PUNP4@iIT341",
        proxy:false,
        name:"purine-nucleoside phosphorylase (Deoxyguanosine)",
        description:"",
        source:"/home/fliu/data/sbm/iIT341.xml",
        orientation:"Reversible",
        translocation:false,
        updated_at:1426458341553,
        created_at:1426458341553,
        containerReversible:true,
        upperBound:999999.0,
        geneRule:"HP1178",
        lowerBound:-999999.0,                                                                 
        model: "iIT341",
        reactants: [{specie: 'specie1', stoichiometry: 1}, {specie: 'specie2', stoichiometry: 2}],
        products: [{specie:'specie3', stoichiometry: 1}, {specie: 'specie4', stoichiometry: 3}]
    },                                               
    {entry:"R_NADK@iIT341",
        proxy:false,
        name:"NAD kinase",
        description:"",
        source:"/home/fliu/data/sbm/iIT341.xml",
        orientation:"LeftToRight",
        translocation:false,
        updated_at:1426458341556,
        created_at:1426458341556,
        containerReversible:false,
        upperBound:999999.0,
        geneRule:"HP1394",
        lowerBound:0.0,                                                                       
        model: "iIT341",
        reactants: [{specie: 'specie1', stoichiometry: 1}, {specie: 'specie2', stoichiometry: 2}],
        products: [{specie:'specie3', stoichiometry: 1}, {specie: 'specie4', stoichiometry: 3}]
    },                                                                                    
    {entry:"R_PUNP5@iIT341",
        proxy:false,
        name:"purine-nucleoside phosphorylase (Inosine)",
        description:"",
        source:"/home/fliu/data/sbm/iIT341.xml",
        orientation:"Reversible",
        translocation:false,
        updated_at:1426458341559,
        created_at:1426458341559,
        containerReversible:true,
        upperBound:999999.0,
        geneRule:"HP1178",
        lowerBound:-999999.0,                                                                 
        model: "iIT341",
        reactants: [{specie: 'specie1', stoichiometry: 1}, {specie: 'specie2', stoichiometry: 2}],
        products: [{specie:'specie3', stoichiometry: 1}, {specie: 'specie4', stoichiometry: 3}]
    },                                                      
    {entry:"R_PTPATi@iIT341",
        proxy:false,
        name:"pantetheine-phosphate adenylyltransferase",
        description:"",
        source:"/home/fliu/data/sbm/iIT341.xml",
        orientation:"LeftToRight",
        translocation:false,
        updated_at:1426458341562,
        created_at:1426458341562,
        containerReversible:false,
        upperBound:999999.0,
        geneRule:"HP1475",
        lowerBound:0.0,                                                                       
        model: "iIT341",
        reactants: [{specie: 'specie1', stoichiometry: 1}, {specie: 'specie2', stoichiometry: 2}],
        products: [{specie:'specie3', stoichiometry: 1}, {specie: 'specie4', stoichiometry: 3}]
    },                                                   
    {entry:"R_H2CO3TP@iIT341",
        proxy:false,
        name:"H2CO3 transport",
        description:"",
        source:"/home/fliu/data/sbm/iIT341.xml",
        orientation:"Reversible",
        translocation:true,
        updated_at:1426458341565,
        created_at:1426458341565,
        containerReversible:true,
        upperBound:999999.0,
        geneRule:"",
        lowerBound:-999999.0,                                                                                                                                                      
        model: "iIT341",
        reactants: [{specie: 'specie1', stoichiometry: 1}, {specie: 'specie2', stoichiometry: 2}],
        products: [{specie:'specie3', stoichiometry: 1}, {specie: 'specie4', stoichiometry: 3}]
    }
    ];

    db.collection('reactions', function(err, collection){
        collection.insert(reactions, {safe: true}, function(err, result) {});
    });    
};

var populateDbSpecies = function(){
    var species = [
    {entry:"M_cytd_b@iIT341",proxy:false,metaboliteClass:"COMPOUND",name:"Cytidine",formula:"C9H13N3O5",description:"",updated_at:1426458340551,created_at:1426458340551,compartment:"Extra Organism"},                              
    {entry:"M_cytd_c@iIT341",proxy:false,metaboliteClass:"COMPOUND",name:"Cytidine",formula:"C9H13N3O5",description:"",updated_at:1426458340562,created_at:1426458340562,compartment:"Cytosol"},                               
    {entry:"M_arg_DASH_L_b@iIT341",proxy:false,metaboliteClass:"COMPOUND",name:"L-Arginine",formula:"C6H15N4O2",description:"",updated_at:1426458340566,created_at:1426458340566,compartment:"Extra Organism"},                      
    {entry:"M_arg_DASH_L_c@iIT341",proxy:false,metaboliteClass:"COMPOUND",name:"L-Arginine",formula:"C6H15N4O2",description:"",updated_at:1426458340569,created_at:1426458340569,compartment:"Cytosol"},                       
    {entry:"M_ara5p_c@iIT341",proxy:false,metaboliteClass:"COMPOUND",name:"D-Arabinose 5-phosphate",formula:"C5H9O8P",description:"",updated_at:1426458340572,created_at:1426458340572,compartment:"Cytosol"},       
    {entry:"M_cytd_e@iIT341",proxy:false,metaboliteClass:"COMPOUND",name:"Cytidine",formula:"C9H13N3O5",description:"",updated_at:1426458340575,created_at:1426458340575,compartment:"Extra Organism"},                
    {entry:"M_gdpddman_c@iIT341",proxy:false,metaboliteClass:"COMPOUND",name:"GDP-4-dehydro-6-deoxy-D-mannose",formula:"C16H21N5O15P2",description:"",updated_at:1426458340578,created_at:1426458340578,compartment:"Cytosol"},
    {entry:"M_dgdp_c@iIT341",proxy:false,metaboliteClass:"COMPOUND",name:"dGDP",formula:"C10H12N5O10P2",description:"",updated_at:1426458340581,created_at:1426458340581,compartment:"Cytosol"},        
    {entry:"M_cmpkdo_c@iIT341",proxy:false,metaboliteClass:"COMPOUND",name:"CMP-3-deoxy-D-manno-octulosonate",formula:"C17H24N3O15P",description:"",updated_at:1426458340584,created_at:1426458340584,compartment:"Cytosol"},
    {entry:"M_ni2_e@iIT341",proxy:false,metaboliteClass:"COMPOUND",name:"nickel",formula:"Ni",description:"",updated_at:1426458340587,created_at:1426458340587,compartment:"Extra Organism"}      ];

    db.collection('species', function(err, collection){
        collection.insert(species, {safe: true}, function(err, result){});
    });
};

var populateDbMetabolites = function(){
    var metabolites = [
    {entry:"mi145p@iMM904",proxy:false,metaboliteClass:"COMPOUND",description:"",updated_at:1426497997973,created_at:1426497997973,species:[{specie:'specie1',model:'iMM904'}], crossreferences: [{name: 'ReferenceName', identifier: 'P20323', link: "http://www.google.pt"}]},
    {entry:"orot5p@iMM904",proxy:false,metaboliteClass:"COMPOUND",description:"",updated_at:1426497997989,created_at:1426497997989,species:[{specie:'specie1',model:'iMM904'}], crossreferences: [{name: 'ReferenceName', identifier: 'P20323', link: "http://www.google.pt"}]},   
{entry:"hmbil@iMM904",proxy:false,metaboliteClass:"COMPOUND",description:"",updated_at:1426497997992,created_at:1426497997992,species:[{specie:'specie1',model:'iMM904'}], crossreferences: [{name: 'ReferenceName', identifier: 'P20323', link: "http://www.google.pt"}]},
{entry:"23camp@iMM904",proxy:false,metaboliteClass:"COMPOUND",description:"",updated_at:1426497997995,created_at:1426497997995,species:[{specie:'specie1',model:'iMM904'}], crossreferences: [{name: 'ReferenceName', identifier: 'P20323', link: "http://www.google.pt"}]},
{entry:"ertascb-D@iMM904",proxy:false,metaboliteClass:"COMPOUND",description:"",updated_at:1426497997997,created_at:1426497997997,species:[{specie:'specie1',model:'iMM904'}], crossreferences: [{name: 'ReferenceName', identifier: 'P20323', link: "http://www.google.pt"}]},
{entry:"hxc2coa@iMM904",proxy:false,metaboliteClass:"COMPOUND",description:"",updated_at:1426497997999,created_at:1426497997999,species:[{specie:'specie1',model:'iMM904'}], crossreferences: [{name: 'ReferenceName', identifier: 'P20323', link: "http://www.google.pt"}]},
{entry:"coucoa@iMM904",proxy:false,metaboliteClass:"COMPOUND",description:"",updated_at:1426497998001,created_at:1426497998001,species:[{specie:'specie1',model:'iMM904'}], crossreferences: [{name: 'ReferenceName', identifier: 'P20323', link: "http://www.google.pt"}]},
{entry:"2obut@iMM904",proxy:false,metaboliteClass:"COMPOUND",description:"",updated_at:1426497998004,created_at:1426497998004,species:[{specie:'specie1',model:'iMM904'}], crossreferences: [{name: 'ReferenceName', identifier: 'P20323', link: "http://www.google.pt"}]},
{entry:"lystrna@iMM904",proxy:false,metaboliteClass:"COMPOUND",description:"",updated_at:1426497998006,created_at:1426497998006,species:[{specie:'specie1',model:'iMM904'}], crossreferences: [{name: 'ReferenceName', identifier: 'P20323', link: "http://www.google.pt"}]},
{entry:"3ig3p@iMM904",proxy:false,metaboliteClass:"COMPOUND",description:"",updated_at:1426497998008,created_at:1426497998008,species:[{specie:'specie1',model:'iMM904'}], crossreferences: [{name: 'ReferenceName', identifier: 'P20323', link: "http://www.google.pt"}]} 
    ];
  
    db.collection('metabolites', function(err, collection){
        collection.insert(metabolites, {safe: true}, function(err, result){});
    });
};

var populateDbGPRs = function(){
    var gprs = [
    {reaction: "R_AHCi@iMM904", rule: "YER043C", normalized_rule: "YER043C", leafs: [{entry: "S000000845", systematic_name: "YER043C"}]},
    {reaction: "R_PYAM5PO@iMM904", rule: "YBR035C", normalized_rule: "YBR035C", leafs: [{entry: "S000000239", systematic_name: "YBR035C"}]},
    {reaction: "R_SPMDt3i@iMM904", rule: " (YOR273C or YLL028W)", normalized_rule: "(YOR273C OR YLL028W)", leafs: [{entry: "S000005799", systematic_name: "YOR273C"}, {entry: "S000003951", systematic_name: "YLL028W"}]},
    {reaction: "R_ACACT6p@iMM904", rule: "YIL160C", normalized_rule: "YIL160C", leafs: [{entry: "S000001422", systematic_name: "YIL160C"}]},
    {reaction: "R_ACACT1@iMM904", rule: "YPL028W", normalized_rule: "YPL028W", leafs: [{entry: "S000005949", systematic_name: "YPL028W"}]},
    {reaction: "R_SHK3D@iMM904", rule: "YDR127W", normalized_rule: "YDR127W", leafs: [{entry: "S000002534", systematic_name: "YDR127W"}]},
    {reaction: "R_ADMDC@iMM904", rule: "YOL052C", normalized_rule: "YOL052C", leafs: [{entry: "S000005412", systematic_name: "YOL052C"}]},
    {reaction: "R_ARABR@iMM904", rule: "YHR104W", normalized_rule: "YHR104W", leafs: [{entry: "S000001146", systematic_name: "YHR104W"}]} 
    ];

    db.collection('gprs', function(err, collection){
        collection.insert(gprs, {safe: true}, function(err, result){});
    });
};
