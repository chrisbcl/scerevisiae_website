var compartments = {"Golgi Membrane": "gm",
                     "Vacuolar Membrane": "vm",
                     "Endoplasmic Reticulum Membrane": "rm",
                     "Peroxisomal Membrane": "xm",
                     "Cytoplasmic Membrane": "cm",
                     "Cytosol": "c",
                     "Golgi Apparatus": "g",
                     "Extra Cellular": "e",
                     "Nucleus": "n",
                     "Mitochondrion": "m",
                     "Vacuole": "v",
                     "Endoplasmic Reticulum": "r",
                     "Mitochondrial Membrane": "mm",
                     "Peroxisome": "x",
                     "Nuclear Membrane": "nm",
                     "Boundary": "b",
                     "Lipid Particle": "lp",
                     "Cell Envelope": "ce",
		     "Plasma Membrane": "pm"};


var model_list = ["iIN800", 
       "Yeast 6","iTO977",
       "iFF708", 
       "Yeast 1","iAZ900","iND750","iLL672", 
       "Yeast 7", "All"] //o iMM904 ja esta no template


var model_map = {
    "iIN800" : "iIN800",
    "ymn6_06_cobra" : "Yeast 6",
    "iTO977" : "iTO977",
    "iFF708" : "iFF708",
    "ymn1_0" : "Yeast 1",
    "iAZ900" : "iAZ900",
    "MODELID_222668" : "iND750",
    "iLL672" : "iLL672",
    "ymn_7_6_cobra" : "Yeast 7",
    "iMM904" : "iMM904"
}



window.SGDGeneView = Backbone.View.extend({
    
    template: _.template($('#sgdgene-template').html()),

    initialize: function(){
        this.render();
    },

    render: function(){
        $(this.el).html(this.template(this.model.toJSON()));
    }
});


window.SGDGeneListView = Backbone.View.extend({

    template: _.template($('#sgdgenelist-template').html()),    

    initialize: function(){
        this.render();
    },

    render: function(){
        //var genes = this.model.models;
        //var len = genes.length;
        //var startPos = 0;
        //var endPos = len;

        $(this.el).html(this.template);
        //for (var i = startPos; i < endPos; i++){
        //    $('.sgdgene-list', this.el).append(new SGDGeneListItemView({model: genes[i]}).render().el);
        //}   
        //return this;
    }

});


window.SGDGeneListItemView = Backbone.View.extend({
    tagName: 'tr',

    template: _.template($('#sgdgenelistitem-template').html()),

    initialize: function(){
        this.model.bind("change", this.render, this);
        this.model.bind("destroy", this.close, this);
    },

    render:function(){
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    }

});


window.ReactionView = Backbone.View.extend({
    template: _.template($('#reaction-template').html()),

    initialize: function(){
        this.render();
    },

    render: function(){
        $(this.el).html(this.template(this.model.toJSON()));
    }
});

window.ReactionListView = Backbone.View.extend({
    template: _.template($('#reactionlist-template').html()),

    initialize: function(){
        this.render();
    },

    render: function(){
        //var reactions = this.model.models;
        //var len = reactions.length;
        $(this.el).html(this.template);
        /*for (var i = 0; i < len; i++){
            $('.reaction-list', this.el).append(new ReactionListItemView({model: reactions[i]}).render().el);
        }

        return this;*/
    }
});

window.ReactionListItemView = Backbone.View.extend({
    tagName: 'tr',
    
    template: _.template($('#reactionlistitem-template').html()),

    initialize: function(){
        this.model.bind("change", this.render, this);
        this.model.bind("destroy", this.close, this);
    },

    render: function(){
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    }
});


window.SpecieView = Backbone.View.extend({
    template: _.template($('#specie-template').html()),

    initialize: function(){
        this.render();
    },

    render: function(){
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    }
});

window.SpecieListView = Backbone.View.extend({
    template: _.template($('#specielist-template').html()),

    initialize: function(){
        this.render();
    },

    render: function(){
       // var species = this.model.models;
       // var len = species.length;
        $(this.el).html(this.template);
       /* for (var i = 0; i < len; i++){
            $('.specie-list', this.el).append(new SpecieListItemView({ model: species[i]}).render().el);
        }
        return this;*/
    }

});

window.SpecieListItemView = Backbone.View.extend({
    tagName: 'tr',

    template: _.template($('#specielistitem-template').html()),

    initialize: function(){
       this.model.bind("change", this.render, this);
       this.model.bind("destroy", this.close, this);
    },

    render: function(){
        $(this.el).html(this.template(this.model.toJSON()));
        return this;    
    }
});


window.MetaboliteView = Backbone.View.extend({
    template: _.template($('#metabolite-template').html()),

    initialize: function(){
        this.render();
    },

    render: function(){
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    }
});

window.MetaboliteListView = Backbone.View.extend({
    template: _.template($('#metabolitelist-template').html()),

    initialize: function(){
        this.render();
    },

    render: function(){
        var metabolites = this.model.models;
        var len = metabolites.length;
        $(this.el).html(this.template);

        for (var i = 0; i < len; i++){
            $('.metabolite-list', this.el).append(new MetaboliteListItemView({ model: metabolites[i]}).render().el);
        }
    }
});

window.MetaboliteListItemView = Backbone.View.extend({
    tagName: 'tr',

    template: _.template($('#metabolitelistitem-template').html()),

    initialize: function(){
        this.model.bind("change", this.render, this);
        this.model.bind("destroy", this.close, this);
    },

    render: function(){
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    }
});


window.GPRView = Backbone.View.extend({
    template: _.template($('#gpr-template').html()),

    initialize: function(){
        this.render();
    },

    render: function(){
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    }
});

window.HomeView = Backbone.View.extend({
    template: _.template($('#home-template').html()),

    initialize: function(){
        this.render();
    },

    render: function(){
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    }
});

window.CompartmentsView = Backbone.View.extend({
    template: _.template($('#compartments-template').html()),

    initialize: function(){
        this.render();
    },

    render: function(){
        $(this.el).html(this.template());
        return this;
    }
});

window.AboutView = Backbone.View.extend({
    template: _.template($('#about-template').html()),

    initialize: function(){
        this.render();
    },

    render: function(){
        $(this.el).html(this.template());
        return this;
    }
});

var AppRouter = Backbone.Router.extend({
    
    routes : {
        ""                                  : "home",
        "sgdgenes"                          : "geneList",
        "sgdgenes/:entry"                   : "geneDetails",
        "reactions"                         : "reactionList",
        "reactions/:model/:reaction"        : "reactionDetails",
        "species"                           : "specieList",
        "species/:model/:specie"            : "specieDetails",
        "metabolites"                       : "metaboliteList",
        "metabolites/:model/:metabolite"    : "metaboliteDetails",
        "gprs/:model/:reaction"             : "gprDetails",
        "compartments"                      : "compartmentsDetails",
        "about"                             : "aboutDetails"
    },

    initialize: function(){
        //this.headerView = new HeaderView();
        //$('.header').html(this.headerView.el);
    },  

    compartmentsDetails: function(){
        $('#main-content').html(new CompartmentsView().el);
    },

    aboutDetails: function(){
        $('#main-content').html(new AboutView().el);
    },

    home: function(){
        /*if (!this.homeView){
            this.homeView = new HomeView();
        }
        $('#main-content').html(this.homeView.el);*/
    },

    geneList: function(){
        //var sgdgeneList = new SGDGeneCollection();
        //sgdgeneList.fetch({success: function(){
        //    $('#main-content').html(new SGDGeneListView({ model: sgdgeneList}).el);
        //}});
        $('#main-content').html(new SGDGeneListView().el);
        initializeGeneTable('#sgdgene-table');
        //initializeTable("#sgdgene-table");
    },

    geneDetails: function(entry){
        //if (this.sgdgeneList === null) this.geneList();
        var gene = new SGDGene({entry: entry});
        gene.fetch({success: function(){
            $('#main-content').html(new SGDGeneView({model: gene}).el);  
            initializeGeneReactionsRefs("#sgd-reactionrefs-table");
        }});
    },

    reactionList: function(){
        /*var reactionlist = new ReactionCollection();
        reactionlist.fetch({success: function(){
            $('#main-content').html(new ReactionListView({ model: reactionlist}).el);
        }});*/
        $('#main-content').html(new ReactionListView().el);
        initializeReactionTable("#reaction-table");
        //initializeTable('#reaction-table');
    },
    
    reactionDetails: function(model, reaction){
        //if (this.reactionlist === null) this.reactionList();
        var rx = new Reaction({model: model, reaction : reaction});
        rx.fetch({success: function(){
            $('#main-content').html(new ReactionView({model : rx}).el);
            var metabolites = rx.attributes;
            if ( metabolites.modelrefs.length == 1 && metabolites.modelrefs[0].entry == null){
                initializeReactionModelRefsTable('#modelrefs-table', null, null);
            } else {
                initializeReactionModelRefsTable('#modelrefs-table', model, metabolites);
            }
        }});
    },

    specieList: function(){
        /*var specielist = new SpecieCollection();
        specielist.fetch({success: function(){
            $('#main-content').html(new SpecieListView({ model: specielist}).el);
        }});*/
        $('#main-content').html(new SpecieListView().el);
        initializeSpeciesTable('#specie-table');
        //initializeTable('#specie-table');
    },

    specieDetails: function(model, specie){
        //if (this.specielist === null) this.specieList();
        var spc = new Specie({model: model, specie: specie});
        spc.fetch({success: function(){
            $('#main-content').html(new SpecieView({model: spc}).el);
            initializeSpeciesModelRefsTable('#metabolite-modelrefs-table');
            initializeReactionModelRefsTable('#metabolite-reactionrefs-table', null, null);
        }});
    },

    metaboliteList: function(){
        var metabolitelist = new MetaboliteCollection();
        metabolitelist.fetch({ success: function(){
            $('#main-content').html(new MetaboliteListView({ model: metabolitelist}).el);
        }});
        initializeTable('#metabolite-table');
    },

    metaboliteDetails: function(model, metabolite){
        if (this.metabolitelist === null) this.metabolitelist();
        var metab = new Metabolite({model: model, metabolite: metabolite});
        metab.fetch({ success: function(){
            $('#main-content').html(new MetaboliteView({model: metab}).el);
        }});
    },

    gprDetails: function(model, reaction){
        var gpr = new GPR({model: model, reaction: reaction});
        gpr.fetch({ success: function(){
            $('#main-content').html(new GPRView({model: gpr}).el);
            initializeGeneReactionsRefs("#gpr-reactionrefs-table");
        }});
    }

});

getColors = function(n){
    var h = 0;
    var colors = [];
    for (var i = 0; i < n; i++) {
        var color = new KolorWheel([h,72,57]);
        colors.push(color.getHex());
        h += 35;
    }
    return colors;
}

getEqualMetabolites = function(table, model, data){
    var deferreds = [];
    var results = [];
    var metabolites = data.reactants.concat(data.products).map(function(a){ return a.specie; });
    var n = data.reactants.length + data.products.length;
    var colors = getColors(n);

    for (var i = 0; i < data.reactants.length; i++){
        data.reactants[i].color = colors[i];
    }
    for (var i = 0; i < data.products.length; i++){
        data.products[i].color = colors[i+data.reactants.length];
    }

    var newEquation = getEquation(data, true);
    $('#equation').html(newEquation);

    $.each(metabolites, function(index, metabolite){
        deferreds.push(
            $.ajax({
                url: "metabolite/" + model + "/" + metabolite,
                type: "GET",
                success: function(species){
                    results.push(species);
                    return true;
                }
            })
        );
    });
    $.when.apply($, deferreds).then(function(){
        for (var i = 0; i < data.reactants.length; i++){
            for (var j = 0; j < results.length; j++){
                if (data.reactants[i].specie === results[j].entry.split("@")[0]){
                    data.reactants[i].aliases = results[j].aliases;
                }
            }
        }
        for (var i = 0; i < data.products.length; i++){
            for (var j = 0; j < results.length; j++){
                if (data.products[i].specie === results[j].entry.split("@")[0]){
                    data.products[i].aliases = results[j].aliases;
                }
            }
        }
        updateEquationCells(table, data);

    });
}
 
updateEquationCells = function(table, data){
    var nRows = table.fnGetData().length;
    for (var i = 0; i < data.modelrefs.length; i++){
        if (data.modelrefs[i].entry === null)
            data.modelrefs.splice(i,1);
    }
    
    for (var i = 0; i < nRows; i++){
            updateEquationCell(table, data, i);
    }
}

updateEquationCell = function(table, data, index){
    var model = data.modelrefs[index].entry.split("@")[1];
    var newEquation = [];
    newEquation.entry = data.modelrefs[index].entry;
    newEquation.orientation = data.modelrefs[index].orientation;
    newEquation.reactants = getNewReactants(data, model,index);
    newEquation.products = getNewProducts(data, model, index);
    
    var equationHtml = getEquation(newEquation, true);

    table.fnUpdate(equationHtml, index, 2);
    $('td', table.fnGetNodes(index)).eq(2).attr('data-content', equationHtml);
}

getNewProducts = function(data, model, index){
    var count_prods = data.modelrefs[index].products.length;
    var newProducts = []
    var matched_prods = [];

    for (var i = 0; i < data.products.length; i++){
        if (count_prods > 0) {
            for (var j = 0; j < data.modelrefs[index].products.length; j++){
                if (data.products[i].aliases.indexOf(data.modelrefs[index].products[j].specie + '@' + model) >= 0){
                    newProducts.push( { specie: data.modelrefs[index].products[j].specie,
                        stoichiometry: data.modelrefs[index].products[j].stoichiometry,
                        compartment: data.modelrefs[index].products[j].compartment,
                        alias: data.modelrefs[index].products[j].alias,
                        color: data.products[i].color
                    });
                    count_prods--;
                    matched_prods.push(data.modelrefs[index].products[j].specie);
                }
            }
        } else {
            break;
        }
    }
    if (count_prods > 0){
        for (var i = 0; i < data.modelrefs[index].products.length; i++){
            if (matched_prods.indexOf(data.modelrefs[index].products[i].specie) < 0){
                newProducts.push( { specie: data.modelrefs[index].products[i].specie,
                    stoichiometry: data.modelrefs[index].products[i].stoichiometry,
                    compartment: data.modelrefs[index].products[i].compartment,
                    alias: data.modelrefs[index].products[i].alias,
                    color: "#999966"
                });
            }
        }
    }
    return newProducts;
}

getNewReactants = function(data, model, index){
    var count_reacts = data.modelrefs[index].reactants.length;
    var newReactants = []; 
    matched_reacts = [];
    for (var i = 0; i < data.reactants.length; i++){
        if (count_reacts > 0) {
            for (var j = 0; j < data.modelrefs[index].reactants.length; j++){
                if (data.reactants[i].aliases.indexOf(data.modelrefs[index].reactants[j].specie + '@' + model) >= 0){
                    newReactants.push( { specie: data.modelrefs[index].reactants[j].specie,
                                                  stoichiometry: data.modelrefs[index].reactants[j].stoichiometry,
                                                  alias: data.modelrefs[index].reactants[j].alias,
                                                  color: data.reactants[i].color,
                                                  compartment: data.reactants[i].compartment
                                                });
                    count_reacts--;
                    matched_reacts.push(data.modelrefs[index].reactants[j].specie);
                }
            }
        } else {
            break;
        }
    }
    if (count_reacts > 0){
        for (var i = 0; i < data.modelrefs[index].reactants.length; i++){
            if (matched_reacts.indexOf(data.modelrefs[index].reactants[i].specie) < 0){
                newReactants.push( { specie: data.modelrefs[index].reactants[i].specie,
                                    stoichiometry: data.modelrefs[index].reactants[i].stoichiometry,
                                    alias: data.modelrefs[index].reactants[i].alias,
                                    compartment: data.modelrefs[index].reactants[i].compartment,
                                    color: "#999966"
                                  });
            }
        }
    }
    return newReactants;
}

initializeTable = function(tablename){
    $(tablename).waitUntilExists(function(){
        $(tablename).DataTable({
            "responsive" : true   
        });
    });
    /*table.columns().every( function () {
        var that = this;
                       
        $( '.searchInput').on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        });
    });*/
};

initializeGeneReactionsRefs = function(tablename){
    $(tablename).waitUntilExists(function(){
        var table = $(tablename).dataTable({
            "responsive": true,
            "aoColumns": [
                {
                    "sWidth": "15%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        nTd.title = sData;
                    }
                },
                {
                    "sWidth": "20%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
			if (sData != null && sData != '')                        
				nTd.title = sData;
                    }
                },
                {
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        $(nTd).attr('data-content', sData);
                        $(nTd).popover({
                            html: true,
                            trigger: 'hover',
                            placement: 'top',
                            container: $(nTd)
                        });
                    }
                },
                {
                    "sWidth": "3%",
                    "orderable": false
                }
            ]
        });
    });
}

initializeReactionModelRefsTable = function(tablename, model, data){
    //var dom_value = 'lrtip';
    //if (model !== null && data !== null){
    var dom_value = "lfrtip<'row'>B";
    //}

    $(tablename).waitUntilExists(function(){
        var table = $(tablename).dataTable({
            "responsive": true,
            "aoColumns": [
                {
                    "sWidth": "9%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    }
                }, 
                {
                    "sWidth": "7%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    }
                },
                {
                    "sWidth": "40%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        $(nTd).attr('data-content', sData);
                        $(nTd).popover({
                            html: true,
                            trigger: 'hover',
                            placement: 'top',
                            container: $(nTd)
                        });
                    }
                },
                {
                    "sWidth": "10%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    }
                },
                {
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
			if (sData != null && sData != ''){                        
		                $(nTd).attr('data-content', sData);
		                $(nTd).popover({
		                    html: true,
		                    trigger: 'hover',
		                    placement: 'top',
		                    container: $(nTd)
		                });
			}
                    }
                },
                {
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    }
                },
                {
                    "sWidth": "3%",
                    "orderable": false
                }
            ],
            dom: dom_value,
                buttons: [
                {
                    extend: 'colvisGroup',
                    text: 'Equation',
                    show: [0,1,2,6],
                    hide: [3,4,5]
                }, 
                {
                    extend: 'colvisGroup',
                    text: 'GPR',
                    show: [0,1,4,6],
                    hide: [2,3,5]
                },
                {
                    extend: 'colvisGroup',
                    text: 'Equation+GPR',
                    show: [0,1,2,4,6],
                    hide: [3,5]
                },
                {
                    extend: 'colvisGroup',
                    text: 'Show all',
                    show: ':hidden'
                }
            ]
        });
        if (model !== null && data !== null) getEqualMetabolites(table, model, data);
    });
};

initializeSpeciesModelRefsTable = function(tablename){
    $(tablename).waitUntilExists(function(){
        $(tablename).dataTable({
            "responsive" : true,
            "aoColumns": [
                {
                    "sWidth": "15%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    }      
                },
                {
                    "sWidth" : "15%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    }
                },
                {
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    }
                },
                {
                    "sWidth": "15%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    }
                },
                {
                    "sWidth": "3%",
                    "orderable": false
                }
            ]
        });
    });
};

initializeSpeciesTable = function(tablename){
    $(tablename).waitUntilExists(function(){
        $(tablename).dataTable({
            "responsive"  : true,
            "bProcessing" : true,
            "bServerSide" : true,
            "bStateSave": true,
            "sAjaxSource" : 'species',
            "aoColumns" : [
                {   "sWidth": "15%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    },
                    mData: function(source, type, val){
                    var spc = source.entry.split("@")[0];
                    return spc;
                 }
                },

                {
                   "sWidth": "15%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    },
                    mData: function(source, type, val){
                        var alias = source.alias;
                        return alias;
                    }
                },
                {  "sWidth": "10%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    },
                    mData: function(source, type, val){
                    var model = model_map[source.entry.split("@")[1]];
                    return model;
                 }
                },
                { "fnCreatedCell" : function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                  },
                  mData: function(source, type, val){
                    return source.name;
                 }
                },
                { "sWidth": "15%",
                  "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                  },
                  mData: function(source, type, val){
                    return source.compartment;
                 }
                },
                { "sWidth": "3%",
                  "orderable": false,
                  mData: function(source, type, val){
                    var spc = source.entry.split("@")[0];
                    var model = source.entry.split("@")[1];
                    var more = '<a href="#species/'+ model + '/' + spc + '"><span class="fa fa-search"></span></a>';
                    return more;
                 }
                }
               ]
        }).columnFilter({
                        aoColumns : [
                                        null,
                                        null,
                                    {
                                        type: "select",
                                        values : model_list
                                    },
                                    null,
                                    null,
                                    null
                                    ]
        });
    });
}

initializeGeneTable = function(tablename){
    $(tablename).waitUntilExists(function(){
        $(tablename).dataTable({
             "responsive"  : true,
             "bProcessing" : true,
             "bServerSide" : true,
             "bStateSave": true,
             "sAjaxSource" : 'sgdgenes',
             "aoColumns" : [
                { mData: function(source, type, val){
                            return source.entry;
                         }
                },
                { mData: function(source, type, val){
                            if (source.systematic_name === null || 
                                source.systematic_name === 'null') return '';
                            return source.systematic_name;
                         }
                },
                { mData: function(source, type, val){
                            if (source.name === null || 
                                source.name === 'null') return '';
                            return source.name;
                         }
                },
                {   "orderable": false,
                    mData: function(source, type, val){
                            var entry = source.entry;
                            var more = '<a href="#sgdgenes/'+ entry + '"><span class="fa fa-search"></span></a>';
                            return more;
                         }
                }
             ]
        });
    });
}

initializeReactionTable = function(tablename){
    var dom_value = "lfrtip<'row'>B";
    $(tablename).waitUntilExists(function(){
        $(tablename).dataTable({
            "responsive"  : true,
            "bProcessing" : true,
            "bServerSide" : true,
            "bStateSave": true,
            "sAjaxSource" : 'reactions',
            "aoColumns" : [
                {   "sWidth": "9%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    },
                    mData: function(source, type, val){
                        var rx = source.entry.split("@")[0];
                        return rx;
                    }                                   
                },
                {   "sWidth": "7%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    },
                    mData:  function(source,type,val){
                        var model = model_map[source.entry.split("@")[1]];                
                        return model;
                  }
                },
                {   "sWidth": "33%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        //var equationTooltip = sData.replace(/<([a-z]|[\/])[^>]*>/g, ' ');
                        $(nTd).attr('data-content', sData);
                        $(nTd).popover({
                            html: true,
                            viewport: '#reaction-table_wrapper',
                            trigger: 'hover',
                            placement: 'right',
                            container: $(nTd)
                        });
                    },
                    mData:  function(source,type,val){
                        var equation = getEquation(source, false);
                        return equation;                        
                 }
                },
                {  "sWidth": "10%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    },
                    mData:  function(source,type,val){
                        var ec = source.ecnumber;
                        if (ec === "") return null;
                        return ec;                        
                 }
                },
                {   "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
			if (sData != null && sData != ''){                        
				$(nTd).attr('data-content', sData);
		                $(nTd).popover({
				    html: true,
		                    trigger: 'hover',
		                    placement: 'left',
		                    container: $(nTd)
		                });
			}
                    },
                    mData:  function(source,type,val){
						var model = source.entry.split('@')[1];
						var rx = source.entry.split('@')[0];
                        var gpr_norm = source.gpr_normalized;
                        var gpr = source.gpr_rule;
                        if (gpr_norm !== null){
                            return '<a href="#gprs/' + model + '/' + rx + '" target = "_blank">' + gpr_norm + '</a>';
                        } else if (gpr != "")
                        	return '<a href="#gprs/' + model + '/' + rx + '" target = "_blank">' + gpr + '</a>'; 
			return null;                       
                 }
                },
                {   "sWitdh": "10%",
                    "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    },
                    mData: function(source, type, val){
                        var pathway = source.pathway;
                        return pathway;
                    }
                },
                {   "fnCreatedCell": function(nTd, sData, oData, iRow, iCol){
                        if (sData != null && sData != '') nTd.title = sData;
                    },
                    mData:  function(source,type,val){
                        var name = source.name;
                        return name;                        
                 }
                },
                {   "sWidth": "1.5%",
                    "orderable": false,
                    mData:  function(source,type,val){
                        var model = source.entry.split("@")[1];
                        var rx = source.entry.split("@")[0];
                        var more = '<a href="#reactions/'+ model + '/' + rx + '"><span class="fa fa-search"></span></a>';
                        return more;
                 }
                }
            ],
                dom: dom_value,
                buttons: [
                {
                    extend: 'colvisGroup',
                    text: 'Equation',
                    show: [0,1,2,7],
                    hide: [3,4,5,6]
                },
                {
                    extend: 'colvisGroup',
                    text: 'GPR',
                    show: [0,1,4,7],
                    hide: [2,3,5,6]
                },
                {
                    extend: 'colvisGroup',
                    text: 'Equation+GPR',
                    show: [0,1,2,4,7],
                    hide: [3,5,6]
                },
                {
                    extend: 'colvisGroup',
                    text: 'Show all',
                    show: ':hidden'
                }
            ]
        }).columnFilter({
            aoColumns : [
                            null,
                            {
                                type: "select",
                                values: model_list
                            },
                            null,
                            null,
                            null,
                            null,
                            null
                        ]  
        });
    });
}


getEquation = function(data, color){
    var model = data.entry.split("@")[1];
    var equation = '';
    var reactComps = data.reactants.map(function(a){ return a.compartment; });
    var prodComps = data.products.map(function(a){ return a.compartment; });
    var comps = reactComps.concat(prodComps).unique();
    var differentComps = comps.length > 1;
    var comp_color = '#ff9900';
    
    if (!differentComps) equation = '<span title="' + comps[0] + '" class="btn-primary btn-xs" style="font-size: small; background-color: ' + comp_color + ';">' + compartments[comps[0]] + '</span>&nbsp';

        for (var i = 0; i < data.reactants.length; i++){
            var style = '';
            if (color){
                style = 'style="background-color: ' + data.reactants[i].color + ';"';
            }
            var react = '';
            if (data.reactants[i].alias === null) react = data.reactants[i].specie;
            else react = data.reactants[i].alias;
            var stoic = '';
            if (data.reactants[i].stoichiometry != 1){
                stoic = data.reactants[i].stoichiometry;
            }
            equation = equation + '<span class="btn-xs" style="padding: 0;"> ' + stoic + ' </span><a title="' + data.reactants[i].specie + '" class="btn-primary btn-xs" ' + style + ' href="#species/' + model + '/' + data.reactants[i].specie+ '">' + react + '</a>'; 
            
             if (differentComps) equation = equation + '<span title="' + data.reactants[i].compartment + '" class="btn-primary btn-xs" style="background-color: ' + comp_color + ';">' + compartments[data.reactants[i].compartment] + '</span>';
            
            if (i !== data.reactants.length -1){
                equation = equation + ' + ';
            }            
        }
        if (data.orientation == "LeftToRight") {
            equation = equation + ' => ';
        } else if (data.orientation == "Reversible"){
            equation = equation + ' <=> ';
        } else if (data.orientation == "RightToLeft"){
            equation = equation + ' <= ';
        } else {
            equation = equation + ' ? ';
        }
        
        for (var i = 0; i < data.products.length; i++){
            var style = '';
            var prod = '';
            
            if (data.products[i].alias === null) prod = data.products[i].specie;
            else prod = data.products[i].alias;

            if (color){
                style = 'style="background-color: ' + data.products[i].color + ';"';
            } 
            
            var stoic = '';
            if (data.products[i].stoichiometry != 1){
                stoic = data.products[i].stoichiometry;
            }
            equation = equation + '<span class="btn-xs" style="padding: 0;"> ' + stoic + ' </span><a title="' + data.products[i].specie + '" class="btn-primary btn-xs" ' + style + ' href="#species/' + model + '/' + data.products[i].specie + '">' + prod + '</a>';
            
            if (differentComps)  equation = equation + '<span title="' + data.products[i].compartment + '" class="btn-primary btn-xs" style="background-color: ' + comp_color + ';">' + compartments[data.products[i].compartment] + '</span>';
            
            if (i !== data.products.length -1){
                equation = equation + ' + ';
            }
        }
        return equation;
}

getGeneRef = function(database, identifier){
    if (database == "Uniprot"){
        return 'http://www.uniprot.org/uniprot/' + identifier;
    } 
    if (database == 'Expasy'){
        return 'http://enzyme.expasy.org/EC/' + identifier;
    }
}

getMetaboliteRef = function(crossref){
    if (crossref.database == 'LigandCompound'){
        return 'http://www.kegg.jp/dbget-bin/www_bget?cpd:' + crossref.identifier;    
    }
    if (crossref.database == 'LigandGlycan') {
        return 'http://www.kegg.jp/dbget-bin/www_bget?gl:' + crossref.identifier;
    }
    if (crossref.database == 'LigandDrug') {
        return 'http://www.kegg.jp/dbget-bin/www_bget?dr:' + crossref.identifier;
    }
    if (crossref.database == 'ChEBI') {
        return 'http://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI%3A' + crossref.identifier;
    }
    if (crossref.database == 'MetaCyc') {
        var pgdb = crossref.identifier.split(":");
        return 'http://biocyc.org/' + pgdb[0] + '/NEW-IMAGE?type=COMPOUND&object=' + pgdb[1];
    }
    if (crossref.database == 'Seed') {
        return 'http://seed-viewer.theseed.org/seedviewer.cgi?page=CompoundViewer&compound=' + crossref.identifier + '&model=';
    }
    if (crossref.database == 'PubChemSubstance') {
        return 'http://pubchem.ncbi.nlm.nih.gov/substance/' + crossref.identifier + '?from=summary';
    }
    if (crossref.database == 'PubChemCompound') {
        return 'https://pubchem.ncbi.nlm.nih.gov/compound/' + crossref.identifier;
    }
    if (crossref.database == 'BiGG'){
        return 'http://bigg.ucsd.edu/universal/metabolites/' + crossref.identifier;
    }
    if (crossref.database == 'HMDB'){
        return 'http://hmdb.ca/metabolites/' + crossref.identifier;
    }

}

getReactionRef = function(crossref){
    if (crossref.database == 'LigandReaction'){
        return 'http://www.kegg.jp/dbget-bin/www_bget?rn:' + crossref.identifier;
    }
    if (crossref.database == 'MetaCyc') {
        var pgdb = crossref.identifier.split(":");
        return 'http://biocyc.org/' + pgdb[0] + '/NEW-IMAGE?type=REACTION&object=' + pgdb[1];
    }
    if (crossref.database == 'Seed') {
        return 'http://seed-viewer.theseed.org/seedviewer.cgi?page=ReactionViewer&reaction=' + crossref.identifier + '&model=';
    }
    if (crossref.database == 'BiGG') {
        return 'http://bigg.ucsd.edu/universal/reactions/' + crossref.identifier;
    }
}
/*
getReactionRefs = function(crossrefs){
    var links = [];
    for (var i = 0; i < crossrefs.identifiers.length; i++){
        var crossref;
        crossref.database 
        links.push(getReactionRef(crossrefs.identifiers[i]);
    }
}*/

getBrendaRef = function(ecnumber){
    var patt = new RegExp("[0-9-]+\.[0-9-]+\.[0-9-]+\.[0-9-]+");
    var res = patt.test(ecnumber);
    if (res){
        return 'http://www.brenda-enzymes.org/enzyme.php?ecno='+ecnumber;
    } else {
        return null;
    }
}

getLinkGeneGPR = function(gpr, genes){
    gene_map = {};
    for (var i = 0; i < genes.length; i++){
        if (genes[i].sgdgene !== null){
            gene_map[genes[i].leaf.split("@")[0]] = genes[i].sgdgene;
        }
    }
    final_gpr = gpr;
    names = Object.keys(gene_map);
    for (var i = 0; i < names.length; i++){
        var re = new RegExp(names[i], 'g');
        gpr = gpr.replace( re , '<a href="#sgdgenes/' + gene_map[names[i]] + 
                                       '">' + names[i] + '</a >');
    }
    return gpr;
}

getAggregatedCrossReferences = function(crossrefs){
    var dbs = crossrefs.map(function(a){ return a.database; }).unique().remove(null);
    var newCross = [];
    var finalCross = [];
    for (var i = 0; i < dbs.length; i++){
        newCross.push({ database: dbs[i], identifiers: []});
        for (var j = 0; j < crossrefs.length; j++){
            if (crossrefs[j].database == dbs[i]){
                newCross[i].identifiers.push(crossrefs[j].identifier); 
            }
        }
    }
    var maindbs = dbs.map(function(a){ return getDatabaseName(a); }).unique();
    
    for (var i = 0; i < maindbs.length; i++){
        finalCross.push({maindatabase: maindbs[i], crossreferences: []});
        for (var j = 0; j < newCross.length; j++){
            if (maindbs[i] == getDatabaseName(newCross[j].database)){
                finalCross[i].crossreferences.push(newCross[j]);
            }
        }
    }
    return finalCross;
}

getDatabaseName = function(database){
    if (database == "BiGG"){
        return "BiGG";
    } else if (database == "LigandReaction") {
        return "KEGG";
    } else if (database == "ChEBI") {
        return "ChEBI";
    } else if (database == "Seed") {
        return "Seed";
    } else if (database == "LigandDrug"){
        return "KEGG";
    } else if (database == "MetaCyc"){
        return "MetaCyc";
    } else if (database == "LigandCompound") {
        return "KEGG";
    } else if (database == "PubChemSubstance") {
        return "PubChem";
    } else if (database == "LigandGlycan") {
        return "KEGG";
    } else if (database == "PubChemCompound") {
        return "PubChem";
    } else if (database == "HMDB"){
        return "HMDB";
    }
    return null;
}

filterSameGPRs = function(reaction, maingpr, sameGprs){
    var filtered_gprs = [];
    for (var i = 0; i < sameGprs.length; i++){
        if (sameGprs[i].reaction !== reaction){
            var genes = sameGprs[i].gpr.match(/[^(OR)(AND)\s]+[A-Z0-9]+/g);
            genes.sort();
            if (testIfEqualGPR(maingpr, genes)){
                 filtered_gprs.push(sameGprs[i]);
            }
        }
    }
    return filtered_gprs;
}

testIfEqualGPR = function(maingpr, samegpr){
    var main_genes = maingpr.match(/[^(OR)(AND)\s]+[A-Z0-9]+/g);
    main_genes.sort();
    if (main_genes.length !== samegpr.length) return false;
    else {
        for (var i = 0; i < main_genes.length; i++){
            if (main_genes[i] !== samegpr[i]) return false;
        }
    }
    return true;
}

//$(document).ready(function(){
    var app = new AppRouter();
    Backbone.history.start();
//});


