window.SGDGeneView = Backbone.View.extend({
    
    template: _.template($('#sgdgene-template').html()),

    initialize: function(){
        this.render();
    },

    render: function(){
        $(this.el).html(this.template(this.model.toJSON()));
    }


    

});


SGDGeneListView = Backbone.View.extend({

    template: _.template($('#sgdgenelist-template').html()),    

    initialize: function(){
        this.render();
    },

    render: function(){
        var genes = this.model.models;
        var len = genes.length;
        var startPos = 0;
        var endPos = len;

        console.log(genes);
        $(this.el).html(this.template);

        for (var i = startPos; i < endPos; i++){
            $('.sgdgene-list', this.el).append(new SGDGeneListItemView({model: genes[i]}), render().el);
        }
        

        return this;
    }

});


window.SGDGeneListItemView = Backbone.View.extend({
    
    template: _.template($('#sgdgenelistitem-template').html()),

    initialize: function(){
        this.model.bind("change", this.render, this);
        this.model.bind("destroy", this.close, this);
    },

    render:function(){
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    }

});
