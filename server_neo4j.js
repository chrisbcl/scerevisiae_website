var express = require('express'),
    path = require('path'),
    http = require('http'),
    logger = require('dev-logger'),
    bodyParser = require('body-parser'),
    routes = require('./routes/routes_neo4j'),
    redis_config = require('./redis/redis_config');

var app = express();

    app.set('port', 3001);
    app.use(express.logger('dev'));
    app.use(bodyParser.urlencoded({
        extended: true 
    }));
    app.use(bodyParser.json());
    app.use(express.static(path.join(__dirname, 'public')));

app.get('/sgdgenes', routes.findAllGenes);
app.get('/sgdgenes/:entry', routes.findGeneByEntry);

//app.get('/reactions', routes.findAllReactions);
app.get('/reactions', redis_config.getReactions);
app.get('/reactions/:model/:reaction', routes.findReactionByEntry);

app.get('/species', redis_config.getMetabolites);
//app.get('/species', routes.findAllSpecies);
app.get('/species/:model/:specie', routes.findSpecieByEntry);

app.get('/metabolite/:model/:specie', routes.findEqualMetabolites);
//app.get('/getbiggs', routes.findAllBiggs);
/*
app.get('/metabolites', routes.findAllMetabolites);
app.get('/metabolites/:model/:metabolite', routes.findMetaboliteByEntry);
*/
app.get('/gprs/:model/:reaction', routes.findGPRByReaction);

http.createServer(app).listen(app.get('port'), function(){
    console.log("Express server listening on port " + app.get('port'));
});
